import { Module } from '@nestjs/common';
import { BookingsService } from './services/bookings.service';

@Module({
  providers: [BookingsService],
  exports: [BookingsService],
})
export class BookingsModule {}
