import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseModel } from '../../../common/models/base.model';
import { Flight } from '../../flights/entities/flight.entity';
import { Booking } from './booking.entity';

@Entity()
export class BookedFlight extends BaseModel {
  /** Booked flight. */
  @ManyToOne(type => Flight, { nullable: false })
  flight: Flight;

  /** Booking information. */
  @ManyToOne(type => Booking, { nullable: false })
  booking: Booking;

  /** Number of seats booked. */
  @Column({ type: 'int' })
  seats: number;
}
