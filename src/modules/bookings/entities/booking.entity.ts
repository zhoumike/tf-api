import { Column, Entity } from 'typeorm';
import { BaseModel } from '../../../common/models/base.model';

@Entity()
export class Booking extends BaseModel {
  /** Customer who made the Booking. */
  @Column({ type: 'varchar', length: 255 })
  customer: string;
}
