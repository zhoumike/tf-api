import { Injectable } from '@nestjs/common';

@Injectable()
export class BookingsService {
  /**
   * Returns SQL query that calculates total number of booked seats per flight.
   * @returns SQL query with these selects: `bookedSeats`, `flightId`
   */
  getBookedSeatsQuery(): string {
    return `
      SELECT
        SUM(seats) AS bookedSeats
        ,flightId
      FROM booked_flight
      GROUP BY flightId
    `;
  }
}
