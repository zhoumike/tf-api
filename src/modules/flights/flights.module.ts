import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookingsModule } from '../bookings/bookings.module';
import { Flight } from './entities/flight.entity';
import { FlightsResolver } from './resolvers/flights.resolver';
import { FlightsService } from './services/flights.service';

@Module({
  imports: [TypeOrmModule.forFeature([Flight]), BookingsModule],
  providers: [FlightsResolver, FlightsService],
  exports: [FlightsService],
})
export class FlightsModule {}
