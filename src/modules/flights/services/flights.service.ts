import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as moment from 'moment';
import { Repository } from 'typeorm';
import { BookingsService } from '../../bookings/services/bookings.service';
import { FlightsArgs } from '../dtos/flights.args';
import { Flight } from '../entities/flight.entity';
import { FlightsResponse } from '../models/flights-response.model';
import { RoundTripFlight } from '../models/round-trip-flight.model';

@Injectable()
export class FlightsService {
  /** Minimum time (in minutes) required between departure flight landing and return flight liftoff. */
  private readonly MIN_TIME_BETWEEN_FLIGHTS = 60 * 2;

  /** Date format for Moment and database. */
  private readonly DATE_FORMAT = 'YYYY-MM-DD';

  /** Time format for Moment and database. */
  private readonly TIME_FORMAT = 'hh:mm';

  constructor(
    @InjectRepository(Flight)
    private flightRepository: Repository<Flight>,
    private bookingsService: BookingsService,
  ) {}

  /**
   * Search round-trip flights.
   * @param flightArgs
   * @returns Available round-trip flights.
   */
  async find(flightArgs: FlightsArgs): Promise<FlightsResponse> {
    const result = await this.flightRepository.query(
      `
        -- f1 = origin/departure flight
        -- f2 = destination/return flight
        SELECT
          f1.id AS f1Id
          ,f2.id AS f2Id
          ,f1.departureDate AS f1DepartureDate
          ,f2.departureDate AS f2DepartureDate
          ,f1.duration AS f1Duration
          ,f2.duration AS f2Duration
          ,f1.ticketPrice + f2.ticketPrice AS price
          ,LEAST(f1.seatCapacity - Ifnull(bf1.bookedSeats, 0), f2.seatCapacity - Ifnull(bf2.bookedSeats, 0)) AS availableSeats

        FROM flight f1

        -- join to itself (flight) to create list of round-trip flights (at same two airports)
        -- [args.originAirportCode]
        -- [this.destinationAirportCode]
        INNER JOIN flight f2
          ON f1.departureAirportCode = f2.arrivalAirportCode
          AND f1.arrivalAirportCode = f2.departureAirportCode
          AND f1.departureAirportCode = ?
          AND f1.arrivalAirportCode = ?

        -- add number of booked seats for f1
        LEFT JOIN (${this.bookingsService.getBookedSeatsQuery()}) bf1
          ON bf1.flightId = f1.id

        -- add number of booked seats for f2
        LEFT JOIN (${this.bookingsService.getBookedSeatsQuery()}) bf2
          ON bf2.flightId = f2.id

        -- both flights have at least a single seat available
        WHERE f1.seatCapacity - Ifnull(bf1.bookedSeats, 0) >= 1
          AND f2.seatCapacity - Ifnull(bf2.bookedSeats, 0) >= 1

        -- f1 must land before before f2 departs with a safe margin
        -- [this.MIN_TIME_BETWEEN_FLIGHTS]
          AND DATE_ADD(f1.arrivalDate, INTERVAL ? MINUTE) < f2.departureDate

        -- date of departure flight (f1) and return flight (f2)
        -- [args.departureDate]
        -- [args.departureDate + 1 day]
        -- [args.returnDate]
        -- [args.returnDate + 1 day]
          AND ? <= f1.departureDate AND f1.departureDate < ?
          AND ? <= f2.departureDate AND f2.departureDate < ?

        -- order by ascending flight duration, then flight ids
        -- [args.offset]
        -- [args.limit]
        ORDER BY (f1.duration + f2.duration) ASC,
                  f1.id ASC,
                  f2.id ASC
        LIMIT ?, ?
      `,
      [
        flightArgs.originAirportCode,
        flightArgs.destinationAirportCode,
        this.MIN_TIME_BETWEEN_FLIGHTS,
        `${flightArgs.departureDate} 00:00:00`,
        `${moment(flightArgs.departureDate)
          .add(1, 'day')
          .format(this.DATE_FORMAT)} 00:00:00`,
        `${flightArgs.returnDate} 00:00:00`,
        `${moment(flightArgs.returnDate)
          .add(1, 'day')
          .format(this.DATE_FORMAT)} 00:00:00`,
        flightArgs.offset,
        flightArgs.limit + 1, // add 1 to check if there is a next page
      ],
    );

    // calculate next page offset and remove extra row used for checking if there is a next page
    const nextPageOffset =
      result.length > flightArgs.limit
        ? flightArgs.offset + flightArgs.limit
        : null;
    if (nextPageOffset) {
      result.pop();
    }
    // calculate prev page offset
    let prevPageOffset =
      flightArgs.offset - flightArgs.limit > 0
        ? flightArgs.offset - flightArgs.limit
        : null;
    if (flightArgs.offset > 0 && prevPageOffset === null) {
      prevPageOffset = 0;
    }

    // map query results to RoundTripFlight
    const flights: RoundTripFlight[] = result.map(flight => {
      const departureMoment = moment(flight.f1DepartureDate);
      const returnMoment = moment(flight.f2DepartureDate);

      return {
        departureFlight: {
          date: departureMoment.format(this.DATE_FORMAT),
          time: departureMoment.format(this.TIME_FORMAT),
          duration: this.formatDuration(flight.f1Duration),
        },
        returnFlight: {
          date: returnMoment.format(this.DATE_FORMAT),
          time: returnMoment.format(this.TIME_FORMAT),
          duration: this.formatDuration(flight.f2Duration),
        },
        price: flight.price,
        availableSeats: flight.availableSeats,
      } as RoundTripFlight;
    });

    return { results: flights, prevPageOffset, nextPageOffset };
  }

  /**
   * Formats duration in minutes to "__h __m".
   * @param duration in minutes
   * @returns formatted duration
   */
  private formatDuration(duration: number): string {
    return `${Math.floor(duration / 60)}h ${duration % 60}m`;
  }
}
