import { Args, Query, Resolver } from '@nestjs/graphql';
import { FlightsArgs } from '../dtos/flights.args';
import { FlightsResponse } from '../models/flights-response.model';
import { RoundTripFlight } from '../models/round-trip-flight.model';
import { FlightsService } from '../services/flights.service';

@Resolver(of => RoundTripFlight)
export class FlightsResolver {
  constructor(private readonly flightsService: FlightsService) {}

  @Query(returns => FlightsResponse, { description: 'Round-trip flights' })
  async flights(@Args() args: FlightsArgs): Promise<FlightsResponse> {
    return this.flightsService.find(args);
  }
}
