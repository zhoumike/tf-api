import { Field, Int, ObjectType } from '@nestjs/graphql';
import { RoundTripFlight } from './round-trip-flight.model';

@ObjectType({
  description: 'Response object for searching round-trip flights.',
})
export class FlightsResponse {
  @Field(type => [RoundTripFlight], {
    description: 'List of available round-trip flights.',
  })
  results: RoundTripFlight[];

  @Field(type => Int, {
    nullable: true,
    description: 'Pagination offset for previous page.',
  })
  prevPageOffset?: number;

  @Field(type => Int, {
    nullable: true,
    description: 'Pagination offset for next page.',
  })
  nextPageOffset?: number;
}
