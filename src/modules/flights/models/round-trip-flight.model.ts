import { Field, Int, ObjectType } from '@nestjs/graphql';
import { FlightInfo } from './flight-info.model';

@ObjectType({ description: 'Scheduled round-trip flight information.' })
export class RoundTripFlight {
  @Field(type => FlightInfo, { description: 'Departure flight information.' })
  departureFlight: FlightInfo;

  @Field(type => FlightInfo, { description: 'Return flight information.' })
  returnFlight: FlightInfo;

  @Field(type => Int, { description: 'Total price for round-trip flights.' })
  price: number;

  @Field(type => Int, { description: 'Number of available seats remaining.' })
  availableSeats: number;

  @Field(type => Int, { description: 'Pagination offset for next page.' })
  nextPageOffset?: number;
}
