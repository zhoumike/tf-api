import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: 'Scheduled flight information.' })
export class FlightInfo {
  @Field({
    description:
      'Scheduled flight departure date in local airport date. Format: "yyyy-mm-dd".',
  })
  date: string;

  @Field({
    description:
      'Scheduled flight departure time in local airport time. Format in 24 hours: "hh:mm".',
  })
  time: string;

  @Field({
    description:
      'Estimated flight duration in format: "__h __m". (e.g. 3h 30m).',
  })
  duration: string;
}
