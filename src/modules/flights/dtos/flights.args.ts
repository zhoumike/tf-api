import { ArgsType, Field, Int } from '@nestjs/graphql';
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsPositive,
  Max,
  Min,
} from 'class-validator';
import { IsAirportCode } from '../../../common/validators/is-airport-code.validator';
import { IsDateStringCustom } from '../../../common/validators/is-date-string-custom';

@ArgsType()
export class FlightsArgs {
  @IsNotEmpty()
  @IsAirportCode()
  @Field({ description: 'IATA airport code of origin location.' })
  originAirportCode: string;

  @IsNotEmpty()
  @IsAirportCode()
  @Field({ description: 'IATA airport code of destination location.' })
  destinationAirportCode: string;

  @IsNotEmpty()
  @IsDateStringCustom()
  @Field({
    description:
      'Date of departure in local airport time with format: yyyy-mm-dd.',
  })
  departureDate: string;

  @IsNotEmpty()
  @IsDateStringCustom()
  @Field({
    description:
      'Date of departure in local airport time with format: yyyy-mm-dd.',
  })
  returnDate: string;

  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  @Field(type => Int, {
    defaultValue: 1,
    description: 'Number of passengers. Default is 1.',
  })
  passengerCount? = 1;

  @IsOptional()
  @Min(0)
  @Field(type => Int, {
    defaultValue: 0,
    description: 'Row offset, default is 0.',
  })
  offset = 0;

  @IsOptional()
  @IsPositive()
  @Min(1)
  @Max(100)
  @Field(type => Int, {
    defaultValue: 5,
    description: 'Rows per page, default is 5.',
  })
  limit = 5;
}
