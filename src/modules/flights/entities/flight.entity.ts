import { Column, Entity, OneToMany } from 'typeorm';
import { BaseModel } from '../../../common/models/base.model';
import { BookedFlight } from '../../bookings/entities/booked-flight.entity';

@Entity()
export class Flight extends BaseModel {
  /** Departure airport's IATA airport code. */
  @Column({ type: 'char', length: 3 })
  departureAirportCode: string;

  /** Arrival airport's IATA airport code. */
  @Column({ type: 'char', length: 3 })
  arrivalAirportCode: string;

  /** Date of departure in local departure airport timezone. */
  @Column({ type: 'datetime' })
  departureDate: Date;

  /** Time of departure in local arrival airport timezone. */
  @Column({ type: 'datetime' })
  arrivalDate: Date;

  /** Timezone UTC offset of departure airport. */
  @Column({ type: 'int' })
  departureTimezone: number;

  /** Timezone UTC offset of arrival airport. */
  @Column({ type: 'int' })
  arrivalTimezone: number;

  /** Flight duration in minutes. */
  @Column({ type: 'int' })
  duration: number;

  /** Price of flight ticket for a single seat. Value in CAD cents. */
  @Column({ type: 'int' })
  ticketPrice: number;

  /** Maximum number of seats this flight has. */
  @Column({ type: 'int' })
  seatCapacity: number;

  /** Booked flight records. */
  @OneToMany(type => BookedFlight, bookedFlight => bookedFlight.flight)
  bookedFlights: BookedFlight[];
}
