import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { GqlModuleOptions, GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConnectionOptions } from 'typeorm';
import { configs } from './configs';
import { ConfigType } from './configs/config-type';
import { BookingsModule } from './modules/bookings/bookings.module';
import { FlightsModule } from './modules/flights/flights.module';

const allModules = [FlightsModule, BookingsModule];

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: configs,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) =>
        configService.get<ConnectionOptions>(ConfigType.ORM),
    }),
    GraphQLModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) =>
        configService.get<GqlModuleOptions>(ConfigType.GRAPHQL),
    }),
    ...allModules,
  ],
  exports: allModules,
})
export class AppModule {}
