import { ValidationPipeOptions } from '@nestjs/common';
import { ConfigType } from './config-type';

export default (): { [ConfigType.VALIDATION]: ValidationPipeOptions } => ({
  [ConfigType.VALIDATION]: {
    forbidNonWhitelisted: true,
    forbidUnknownValues: true,
  },
});
