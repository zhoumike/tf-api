export enum ConfigType {
  APP = 'app',
  ORM = 'orm',
  GRAPHQL = 'graphql',
  VALIDATION = 'validation',
}
