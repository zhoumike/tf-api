import { join } from 'path';
import { ConnectionOptions } from 'typeorm';
import { ConfigType } from './config-type';

export default (): { [ConfigType.ORM]: ConnectionOptions } => ({
  [ConfigType.ORM]: {
    type: 'mysql',
    charset: 'utf8mb4',

    host: process.env.TF_API_DB_HOST,
    port: parseInt(process.env.TF_API_DB_PORT, 10),
    database: process.env.TF_API_DB_NAME,
    username: process.env.TF_API_DB_USER,
    password: process.env.TF_API_DB_PASSWORD,

    entities: [join(__dirname, '../modules/*/entities/*.entity.{ts,js}')],

    synchronize: false,
    logging: false,
  },
});
