import appConfig from './app.config';
import graphqlConfig from './graphql.config';
import ormConfig from './orm.config';
import validationConfig from './validation.config';

export * from './app.config';
export * from './config-type';
export const configs = [appConfig, graphqlConfig, ormConfig, validationConfig];
