import { GqlModuleOptions } from '@nestjs/graphql';
import { ConfigType } from './config-type';

export default (): { [ConfigType.GRAPHQL]: GqlModuleOptions } => ({
  [ConfigType.GRAPHQL]: {
    autoSchemaFile: 'schema.gql',
  },
});
