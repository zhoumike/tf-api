import { ConfigType } from './config-type';

export interface AppConfig {
  /** API listener port. */
  port: number;
}

export default (): { [ConfigType.APP]: AppConfig } => ({
  [ConfigType.APP]: {
    port: parseInt(process.env.TF_API_PORT, 10),
  },
});
