import { ValidationPipe, ValidationPipeOptions } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { AppModule } from './app.module';
import { AppConfig, ConfigType } from './configs';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
  );
  const config = app.get(ConfigService);

  app.useGlobalPipes(
    new ValidationPipe(
      config.get<ValidationPipeOptions>(ConfigType.VALIDATION),
    ),
  );

  // Fastify listens only on localhost 127.0.0.1 by default.
  // Listening to 0.0.0.0 allows listening on other hosts.
  // https://www.fastify.io/docs/latest/Getting-Started/#note
  await app.listen(config.get<AppConfig>(ConfigType.APP).port, '0.0.0.0');
}
bootstrap();
