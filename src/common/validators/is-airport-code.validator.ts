import {
  registerDecorator,
  ValidationOptions,
  ValidationArguments,
} from 'class-validator';

const IATA_AIRPORT_CODE_LENGTH = 3;
const AIRPORT_CODE_TEST_PATTERN = /^[a-zA-Z]+$/;

/**
 * Checks if string is a valid IATA airport code.
 */
export function IsAirportCode(validationOptions?: ValidationOptions) {
  return function (object: unknown, propertyName: string) {
    registerDecorator({
      name: 'isAirportCode',
      target: object.constructor,
      propertyName: propertyName,
      options: {
        message: `${propertyName} must be a valid IATA airport code`,
        ...validationOptions,
      },
      validator: {
        validate(value: any, args: ValidationArguments) {
          return (
            typeof value === 'string' &&
            value.length === IATA_AIRPORT_CODE_LENGTH &&
            AIRPORT_CODE_TEST_PATTERN.test(value)
          );
        },
      },
    });
  };
}
