import {
  registerDecorator,
  ValidationOptions,
  ValidationArguments,
} from 'class-validator';
import * as moment from 'moment';

/**
 * Checks if string is a custom date string in format: YYYY-MM-DD.
 */
export function IsDateStringCustom(validationOptions?: ValidationOptions) {
  return function (object: unknown, propertyName: string) {
    registerDecorator({
      name: 'isDateStringCustom',
      target: object.constructor,
      propertyName: propertyName,
      options: {
        message: `${propertyName} must be a date string with format: YYYY-MM-DD`,
        ...validationOptions,
      },
      validator: {
        validate(value: any, args: ValidationArguments) {
          return moment(value, 'YYYY-MM-DD', true).isValid();
        },
      },
    });
  };
}
