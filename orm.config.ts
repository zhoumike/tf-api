import { ConnectionOptions } from 'typeorm';
import { ConfigType } from './src/configs/config-type';
import ormConfig from './src/configs/orm.config';

// This file is required because TypeORM CLI requires a static configuration object.
export default {
  ...ormConfig()[ConfigType.ORM],

  migrations: ['migrations/*.*'],
  cli: {
    migrationsDir: 'migrations',
  },
} as ConnectionOptions;
