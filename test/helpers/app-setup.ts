import { ValidationPipe, ValidationPipeOptions } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AppModule } from '../../src/app.module';
import { ConfigType } from '../../src/configs';
import { Flight } from '../../src/modules/flights/entities/flight.entity';

/**
 * Setup Nest for testing.
 * @returns Nest app
 */
export const appSetup = async (): Promise<NestFastifyApplication> => {
  const moduleFixture: TestingModule = await Test.createTestingModule({
    imports: [AppModule],
    providers: [{ provide: getRepositoryToken(Flight), useClass: Repository }],
  }).compile();

  const app = moduleFixture.createNestApplication<NestFastifyApplication>(
    new FastifyAdapter(),
  );

  // use validation
  const config = app.get(ConfigService);
  app.useGlobalPipes(
    new ValidationPipe(
      config.get<ValidationPipeOptions>(ConfigType.VALIDATION),
    ),
  );

  // initialize app
  await app.init();
  await app.getHttpAdapter().getInstance().ready();

  return app;
};
