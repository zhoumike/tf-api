import { NestFastifyApplication } from '@nestjs/platform-fastify';

const GRAPHQL_REQUEST_URL = '/graphql';

/**
 * Queries GraphQL endpoint and returns response.
 * @param app Nest Fastify app
 * @param query GraphQL query
 * @returns JSON response
 */
export const request = async (app: NestFastifyApplication, query: string) => {
  const request = await app
    .inject()
    .post(GRAPHQL_REQUEST_URL)
    .payload({ query })
    .end();
  return request.json();
};
