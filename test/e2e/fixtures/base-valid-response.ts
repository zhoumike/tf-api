export const baseValidResponse = {
  data: {
    flights: {
      results: [
        {
          departureFlight: {
            date: '2021-10-15',
            time: '09:00',
            duration: '17h 25m',
          },
          returnFlight: {
            date: '2021-10-20',
            time: '09:40',
            duration: '15h 30m',
          },
          price: 150014,
          availableSeats: 1,
        },
        {
          departureFlight: {
            date: '2021-10-15',
            time: '09:00',
            duration: '17h 25m',
          },
          returnFlight: {
            date: '2021-10-20',
            time: '12:40',
            duration: '15h 40m',
          },
          price: 155009,
          availableSeats: 1,
        },
      ],
      prevPageOffset: null,
      nextPageOffset: null,
    },
  },
};
