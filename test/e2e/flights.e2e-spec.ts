import { NestFastifyApplication } from '@nestjs/platform-fastify';
import { FlightsArgs } from 'src/modules/flights/dtos/flights.args';
import { appSetup } from '../helpers/app-setup';
import { request } from '../helpers/request';
import { baseValidResponse } from './fixtures/base-valid-response';

describe('Flights (e2e)', () => {
  let app: NestFastifyApplication;

  beforeAll(async () => {
    app = await appSetup();
  });

  afterAll(async () => {
    await app.close();
  });

  /**
   * Request for round-trip flights.
   * @param flightsArgs
   * @returns response data
   */
  const reqFlights = async (flightsArgs?: Partial<FlightsArgs>) => {
    const originAirportCode = flightsArgs?.originAirportCode ?? 'YVR';
    const destinationAirportCode = flightsArgs?.destinationAirportCode ?? 'CAN';
    const departureDate = flightsArgs?.departureDate ?? '2021-10-15';
    const returnDate = flightsArgs?.returnDate ?? '2021-10-20';
    const limit =
      flightsArgs?.limit !== undefined ? `,limit:${flightsArgs.limit}` : '';
    const offset =
      flightsArgs?.offset !== undefined ? `,offset:${flightsArgs.offset}` : '';

    return request(
      app,
      `{
        flights(
          originAirportCode: "${originAirportCode}",
          destinationAirportCode: "${destinationAirportCode}",
          departureDate: "${departureDate}",
          returnDate: "${returnDate}"` +
        limit +
        offset +
        `) {
          results {
            departureFlight {
              date,
              time,
              duration
            },
            returnFlight {
              date,
              time,
              duration
            },
            price,
            availableSeats
          },
          prevPageOffset,
          nextPageOffset
        }
      }`,
    );
  };

  it('returns valid data with valid request', async () => {
    const response = await reqFlights();
    expect(response).toMatchObject(baseValidResponse);
    expect(response).not.toHaveProperty('errors');
    expect(response.data.flights.results.length).toBeGreaterThan(1);
  });

  it('returns valid paged data and pagination output with valid pagination input', async () => {
    let response: any;

    response = await reqFlights({ offset: 0, limit: 1 });
    expect(response.data.flights.results[0]).toMatchObject(
      baseValidResponse.data.flights.results[0],
    );
    expect(response.data.flights.prevPageOffset).toBeNull();
    expect(response.data.flights.nextPageOffset).toEqual(1);

    response = await reqFlights({ offset: 1, limit: 1 });
    expect(response.data.flights.results[0]).toMatchObject(
      baseValidResponse.data.flights.results[1],
    );
    expect(response.data.flights.prevPageOffset).toEqual(0);
    expect(response.data.flights.nextPageOffset).toBeNull();

    response = await reqFlights({ offset: 2, limit: 1 });
    expect(response.data.flights.results.length).toEqual(0);
    expect(response.data.flights.prevPageOffset).toEqual(1);
    expect(response.data.flights.nextPageOffset).toBeNull();
  });

  it('returns bad request errors with invalid pagination options', async () => {
    let response: any;

    response = await reqFlights({ offset: -1 });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
    response = await reqFlights({ limit: 0 });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
    response = await reqFlights({ limit: -1 });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
  });

  it('returns bad request errors with invalid airport codes', async () => {
    let response: any;

    // expects 3 letter characters: e.g. "CAN"
    response = await reqFlights({ originAirportCode: '' });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
    response = await reqFlights({ originAirportCode: 123 } as any);
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
    response = await reqFlights({ originAirportCode: '123' });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
    response = await reqFlights({ originAirportCode: 'A' });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
    response = await reqFlights({ originAirportCode: 'ABCD' });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);

    response = await reqFlights({ destinationAirportCode: '' });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
    response = await reqFlights({ destinationAirportCode: 123 } as any);
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
    response = await reqFlights({ destinationAirportCode: '123' });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
    response = await reqFlights({ destinationAirportCode: 'A' });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
    response = await reqFlights({ destinationAirportCode: 'ABCD' });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
  });

  it('returns bad request errors with invalid dates', async () => {
    let response: any;

    // expects strict format: 'YYYY-MM-DD'
    response = await reqFlights({ departureDate: '' });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
    response = await reqFlights({ departureDate: '2021/1/19' });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
    response = await reqFlights({ departureDate: 'tomorrow' });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);

    response = await reqFlights({ returnDate: '' });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
    response = await reqFlights({ returnDate: '2021/1/19' });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
    response = await reqFlights({ returnDate: 'tomorrow' });
    expect(response.errors[0].extensions.response.statusCode).toEqual(400);
  });
});
