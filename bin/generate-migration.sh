#!/bin/bash

MIGRATION_NAME=$1
ts-node -r tsconfig-paths/register --transpile-only ./node_modules/typeorm/cli.js migration:generate --config orm.config.ts --name $MIGRATION_NAME
npx prettier --write migrations
