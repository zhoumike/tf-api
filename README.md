# Tribe Flights API


## Description

Flights API implementation using GraphQL and Nest.js framework.


## Contents
- [Tribe Flights API](#tribe-flights-api)
  - [Description](#description)
  - [Contents](#contents)
  - [Prerequisites](#prerequisites)
  - [Setup](#setup)
  - [Running the API](#running-the-api)
    - [Commands](#commands)
    - [GraphQL](#graphql)
  - [Testing](#testing)
  - [Design](#design)
    - [Design Shortfalls](#design-shortfalls)
    - [Pagination](#pagination)
    - [Entities Relationship Diagram](#entities-relationship-diagram)


## Prerequisites

- Node version 14.x.x
- Docker version >= 20.10.8
- Docker-Compose version >= 1.29.1


## Setup

1. Install Node packages:
    ```bash
    $ npm install
    ```
2. Create environment variables file `.env` in project root. File `.env.sample` is a sample containing all mandatory variables.
3. Start database service in the foreground:
    ```bash
    $ npm run db
    ```
4. Run database migration and seed data:
    ```bash
    $ npm run db:migrate
    ```


## Running the API

### Commands

```bash
# database service (required)
$ npm run db

# api (development)
$ npm run start

# api (watch mode)
$ npm run start:dev

# api (production mode)
$ npm run start:prod
```

### GraphQL

The GraphQL endpoint and playground is at [http://localhost:8081/graphql](http://localhost:8081/graphql).

```bash
# sample query
{
  flights(
    originAirportCode: "yvr",
    destinationAirportCode: "can",
    departureDate: "2021-10-15",
    returnDate: "2021-10-20",
    offset: 0,
    limit: 10
  ) {
    results {
      departureFlight {
        date,
        time,
        duration
      },
      returnFlight{
        date,
        time,
        duration
      },
      price,
      availableSeats
    },
    prevPageOffset,
    nextPageOffset
  }
}

# sample response
{
  "data": {
    "flights": {
      "results": [
        {
          "departureFlight": {
            "date": "2021-10-15",
            "time": "09:00",
            "duration": "17h 25m"
          },
          "returnFlight": {
            "date": "2021-10-20",
            "time": "09:40",
            "duration": "15h 30m"
          },
          "price": 150014,
          "availableSeats": 1
        },
        {
          "departureFlight": {
            "date": "2021-10-15",
            "time": "09:00",
            "duration": "17h 25m"
          },
          "returnFlight": {
            "date": "2021-10-20",
            "time": "12:40",
            "duration": "15h 40m"
          },
          "price": 155009,
          "availableSeats": 1
        }
      ],
      "prevPageOffset": null,
      "nextPageOffset": null
    }
  }
}
```


## Testing

The tests rely on seeded data, as there are no convenient ways of creating data inside tests.

```bash
# database service (required)
$ npm run db

# e2e tests
$ npm run test:e2e
```


## Design

### Design Shortfalls

- Only tracks a single airline: Tribe Flights. When searching for round-trips, it assumes every flight is owned by TF. 
- Only tracks direct flights to and from destination, so cannot have connecting flights, layovers, etc.
- Single tiered ticket pricing, all seats in a flight are priced identically.
- Only uses  `CAD` currency and monetary values are tracked in cents, client has to handle the conversions.

### Pagination

A traditional offset pagination design was implemented. It is extremely slow with large data sets. However, we are expecting no more than 50 rows for a round-trip flight for any set of selected dates.

If we are expecting large data sets, using a cursor-based pagination sorted by unique sequential columns would be the most performant. The current design would not allow a unique sequential column, so a re-design would be required to reap the performance benefits.

### Entities Relationship Diagram

![](docs/db-entities-relationships-v2.jpg)
