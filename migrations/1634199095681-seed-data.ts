/* eslint-disable @typescript-eslint/no-unused-vars */
import { BookedFlight } from 'src/modules/bookings/entities/booked-flight.entity';
import { Booking } from 'src/modules/bookings/entities/booking.entity';
import { Flight } from 'src/modules/flights/entities/flight.entity';
import { MigrationInterface, QueryRunner } from 'typeorm';

export class seedData1634199095681 implements MigrationInterface {
  name = 'seedData1634199095681';

  public async up(queryRunner: QueryRunner): Promise<void> {
    // -------
    // Flights
    // -------

    // YVR -> CAN
    const flight1 = await queryRunner.manager.save(Flight, {
      departureAirportCode: 'YVR',
      arrivalAirportCode: 'CAN',
      departureDate: '2021-10-15 9:00:00',
      arrivalDate: '2021-10-16 18:25:00',
      departureTimezone: -7,
      arrivalTimezone: 8,
      duration: 1045,
      ticketPrice: 70009,
      seatCapacity: 5,
    });
    const flight2 = await queryRunner.manager.save(Flight, {
      departureAirportCode: 'YVR',
      arrivalAirportCode: 'CAN',
      departureDate: '2021-10-15 12:00:00',
      arrivalDate: '2021-10-16 21:30:00',
      departureTimezone: -7,
      arrivalTimezone: 8,
      duration: 1050,
      ticketPrice: 75000,
      seatCapacity: 3,
    });
    const flight3 = await queryRunner.manager.save(Flight, {
      departureAirportCode: 'YVR',
      arrivalAirportCode: 'CAN',
      departureDate: '2021-10-20 12:00:00',
      arrivalDate: '2021-10-21 21:35:00',
      departureTimezone: -7,
      arrivalTimezone: 8,
      duration: 1055,
      ticketPrice: 80003,
      seatCapacity: 10,
    });

    // CAN -> YVR
    const flighta = await queryRunner.manager.save(Flight, {
      departureAirportCode: 'CAN',
      arrivalAirportCode: 'YVR',
      departureDate: '2021-10-20 9:40:00',
      arrivalDate: '2021-10-21 10:10:00',
      departureTimezone: 8,
      arrivalTimezone: -7,
      duration: 930,
      ticketPrice: 80005,
      seatCapacity: 4,
    });
    const flightb = await queryRunner.manager.save(Flight, {
      departureAirportCode: 'CAN',
      arrivalAirportCode: 'YVR',
      departureDate: '2021-10-20 12:40:00',
      arrivalDate: '2021-10-21 13:20:00',
      departureTimezone: 8,
      arrivalTimezone: -7,
      duration: 940,
      ticketPrice: 85000,
      seatCapacity: 4,
    });
    const flightc = await queryRunner.manager.save(Flight, {
      departureAirportCode: 'CAN',
      arrivalAirportCode: 'YVR',
      departureDate: '2021-10-30 12:40:00',
      arrivalDate: '2021-10-31 13:10:00',
      departureTimezone: 8,
      arrivalTimezone: -7,
      duration: 930,
      ticketPrice: 90001,
      seatCapacity: 8,
    });

    // --------
    // Bookings
    // --------

    // flight1, flighta
    const booking1 = await queryRunner.manager.save(Booking, {
      customer: 'mike',
    });
    await queryRunner.manager.save(BookedFlight, {
      flight: flight1,
      booking: booking1,
      seats: 1,
    });
    await queryRunner.manager.save(BookedFlight, {
      flight: flighta,
      booking: booking1,
      seats: 1,
    });

    const booking2 = await queryRunner.manager.save(Booking, {
      customer: 'lawrence',
    });
    await queryRunner.manager.save(BookedFlight, {
      flight: flight1,
      booking: booking2,
      seats: 2,
    });
    await queryRunner.manager.save(BookedFlight, {
      flight: flighta,
      booking: booking2,
      seats: 2,
    });

    // flight2, flightb
    const booking3 = await queryRunner.manager.save(Booking, {
      customer: 'jennifer',
    });
    await queryRunner.manager.save(BookedFlight, {
      flight: flight2,
      booking: booking3,
      seats: 3,
    });
    await queryRunner.manager.save(BookedFlight, {
      flight: flightb,
      booking: booking3,
      seats: 3,
    });
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      DELETE FROM \`booked_flight\`;
    `);
    await queryRunner.query(`
      DELETE FROM \`booking\`;
    `);
    await queryRunner.query(`
      DELETE FROM \`flight\`;
    `);
  }
}
