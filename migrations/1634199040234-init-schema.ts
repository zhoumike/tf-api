import { MigrationInterface, QueryRunner } from 'typeorm';

export class initSchema1634199040234 implements MigrationInterface {
  name = 'initSchema1634199040234';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE \`booking\` (\`id\` int NOT NULL AUTO_INCREMENT, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`customer\` varchar(255) NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `CREATE TABLE \`flight\` (\`id\` int NOT NULL AUTO_INCREMENT, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`departureAirportCode\` char(3) NOT NULL, \`arrivalAirportCode\` char(3) NOT NULL, \`departureDate\` datetime NOT NULL, \`arrivalDate\` datetime NOT NULL, \`departureTimezone\` int NOT NULL, \`arrivalTimezone\` int NOT NULL, \`duration\` int NOT NULL, \`ticketPrice\` int NOT NULL, \`seatCapacity\` int NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `CREATE TABLE \`booked_flight\` (\`id\` int NOT NULL AUTO_INCREMENT, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`seats\` int NOT NULL, \`flightId\` int NOT NULL, \`bookingId\` int NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `ALTER TABLE \`booked_flight\` ADD CONSTRAINT \`FK_ca749d944b019590bd978f0bf87\` FOREIGN KEY (\`flightId\`) REFERENCES \`flight\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`booked_flight\` ADD CONSTRAINT \`FK_9cfd0154f58389d861ba5aa80d6\` FOREIGN KEY (\`bookingId\`) REFERENCES \`booking\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`booked_flight\` DROP FOREIGN KEY \`FK_9cfd0154f58389d861ba5aa80d6\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`booked_flight\` DROP FOREIGN KEY \`FK_ca749d944b019590bd978f0bf87\``,
    );
    await queryRunner.query(`DROP TABLE \`booked_flight\``);
    await queryRunner.query(`DROP TABLE \`flight\``);
    await queryRunner.query(`DROP TABLE \`booking\``);
  }
}
